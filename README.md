# Origami name registry

### Purpose
To authenticate then other party through challenge messages. The name registry itself provides the mechanism for verifying the challenge.

### Requirements

- privateKey: a private key in an acceptable format for [crypto-utils](https://gitlab.com/origami2/crypto-utils/blob/master/README.md). That format is currently PKCS1-PEM.

### Usage

```javascript
var NameRegistry = require('origami-name-registry');

var myRegistry = new NameRegistry(privateKey);

myRegistry
.authorizeNamespace(
  'OtherSide',
  publicKey1
);

myRegistry
.addPublicNamespace(
  'PublicConnections'
);
```