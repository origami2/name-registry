/* global context */

var NameRegistry = require('..');
var assert = require('assert');
var crypto = require('origami-crypto-utils');
var testData = require('./testData');

describe('NameRegistry', function () {
  describe('constructor', function () {
    it('requires private key', function () {
      assert.throws(
        function () {
          new NameRegistry();
        },
        /private key is required/
      );
    });

    it('accepts private key', function () {
      new NameRegistry(testData.pair1.privateKey);
    });
  });

  describe('.listKnownNamespaces', function () {
    it('lists no namespaces at first', function () {

      assert.deepEqual(
        [
        ],
        new NameRegistry(testData.pair1.privateKey).listKnownNamespaces()
      );
    });
  });

  describe('.getPublicKey', function () {
    it('returns public key', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      var publicKey = target.getPublicKey();

      assert.equal(publicKey, testData.pair1.publicKey);
    });
  });

  describe('.addPublicNamespace', function () {
    var target;
    
    beforeEach(function () {
      target = new NameRegistry(testData.pair1.privateKey);
    });
    
    it('requires namespace name', function () {
      assert
      .throws(
        function () {
          target.addPublicNamespace();
        },
        /namespace is required/
      );
    });
    
    it('answer false to .isKnownNamespace', function () {
      assert.equal(false, target.isKnownNamespace('n1'));
    });
    
    it('accepts namespace', function () {
      target.addPublicNamespace('n1');
    });
    
    it('answer true to .isKnownNamespace', function () {
      target.addPublicNamespace('n1');
      
      assert.equal(true, target.isKnownNamespace('n1'));
    });
    
    it('lists it in .listKnownNamespaces', function () {
      target.addPublicNamespace('n1');
      
      assert.deepEqual(
        [
          'n1'
        ],
        target.listKnownNamespaces()
      );
    });
  });

  describe('.authorizeNamespace', function () {
    it('requires namespace', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      assert.throws(
        function () {
          target.authorizeNamespace();
        },
        /namespace is required/
      );
    });

    it('requires public key', function () {
      var target = new NameRegistry(testData.pair1.privateKey);

      assert.throws(
        function () {
          target.authorizeNamespace('ns1');
        },
        /public key is required/
      );
    });

    describe('with valid namespace and public key', function () {
      var target;

      before(function () {
        target = new NameRegistry(testData.pair1.privateKey);
      });

      it('works silently', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
      });

      it('lists namespace in listKnownNamespaces', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
        
        assert.deepEqual(
          [
            'ns1'
          ],
          target.listKnownNamespaces()
        );
      });

      it('lists namespace in isKnownNamespace', function () {
        target.authorizeNamespace('ns1', testData.pair2.publicKey);
        
        assert.equal(
          true,
          target.isKnownNamespace('ns1')
        );
      });
    });
    
    describe('.isKnownNamespace', function () {
      it('returns false on unknown namespace', function () {
        var target = new NameRegistry(testData.pair1.privateKey);
        
        assert.equal(false, target.isKnownNamespace('ns1'));
      });
    });

    describe('.answerChallenge', function () {
      var target;

      before(function () {
        target = new NameRegistry(testData.pair1.privateKey);
      });

      it('requires message', function () {
        assert.throws(
          function () {
            target.answerChallenge();
          },
          /message is required/
        );
      });

      it('requires message.code', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              namespace: 'ns2',
              encryptFor: testData.pair2.publicKey
            });
          },
          /message.code is required/
        );
      });

      it('requires message.namespace', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              code: 'abcd1234',
              encryptFor: testData.pair2.publicKey
            });
          },
          /message.namespace is required/
        );
      });

      it('requires message.encryptFor', function () {
        assert.throws(
          function () {
            target.answerChallenge({
              code: 'abcd1234',
              namespace: 'ns2'
            });
          },
          /message.encryptFor is required/
        );
      });

      it('encrypts and signs answer', function () {
        var response = target.answerChallenge({
          code: 'abcd1234',
          encryptFor: testData.pair2.publicKey,
          namespace: 'ns2'
        });

        assert(response);
        assert(response.data);
        assert.equal(testData.pair1.publicKey, response.signedBy);

        var decrypted = crypto.decryptAndVerify(
          response.data,
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        assert.deepEqual(
          [
            'abcd1234',
            'ns2'
          ],
          decrypted
        );
      });
    });
  });

  describe('.createChallenge', function () {
    var target;

    before(function () {
      target = new NameRegistry(testData.pair1.privateKey);
    });

    it('requires a namespace', function () {
      assert.throws(
        function () {
          target.createChallenge();
        },
        /namespace is required/
      );
    });

    it('returns challenge with namespace, code and token', function () {
      var result = target.createChallenge('ns2');

      assert(result);
      assert(result.code);
      assert.equal(result.namespace, 'ns2');
      assert.equal(result.encryptFor, testData.pair1.publicKey);
    });
  });

  describe('.verifyChallenge', function () {
    var target;
    var sampleMessage;
    var sampleResponse;

    before(function () {
      target = new NameRegistry(testData.pair1.privateKey);
      target.authorizeNamespace('ns2', testData.pair2.publicKey);

      var target2 = new NameRegistry(testData.pair2.privateKey);

      sampleMessage = target.createChallenge('ns2');
      sampleResponse = target2.answerChallenge(sampleMessage);
    });

    it('requires message', function () {
      assert.throws(
        function () {
          target.verifyChallenge();
        },
        /message is required/
      );
    });

    it('requires message.code', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          });
        },
        /message.code is required/
      );
    });

    it('requires message.namespace', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            code: 'abcd1234',
            encryptFor: testData.pair1.publicKey
          });
        },
        /message.namespace is required/
      );
    });

    it('requires response', function () {
      assert.throws(
        function () {
          target.verifyChallenge({
            code: 'abcd1234',
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          });
        },
        /response is required/
      );
    });

    it('returns false on invalid response', function () {
      assert.equal(
        false,
        target.verifyChallenge(
          {
            code: 'abcd1234',
            namespace: 'ns2',
            encryptFor: testData.pair1.publicKey
          },
          {
          }
        )
      );
    });

    it('returns namespace on valid response', function () {
      assert.equal(
        'ns2',
        target.verifyChallenge(
          sampleMessage,
          sampleResponse
        )
      );
    });
  });
  
  describe(
    '.getNamespacePublicKey',
    function () {
      context('private namespace', function () {
        context('known', function () {
          it('returns nothing', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            target.addPublicNamespace('n1')
            
            assert(!target.getNamespacePublicKey('n1'));
          });
        });
      });
      
      context('private namespace', function () {
        context('known', function () {
          it('returns public key', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            target.authorizeNamespace('n1', testData.pair2.publicKey);
            
            assert.equal(testData.pair2.publicKey, target.getNamespacePublicKey('n1'));
          });
        });
      
        context('unknown', function () {
          it('throws error', function () {
            var target = new NameRegistry(testData.pair1.privateKey);
            
            assert.throws(
              function () {
                target.getNamespacePublicKey('n1');
              },
              /unknown namespace/
            );
          });
        });
      });
    }
  );
});
