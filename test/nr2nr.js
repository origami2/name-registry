var assert = require('assert');
var testData = require('./testData');
var NameRegistry = require('..');

describe('NameRegistry to NameRegistry', function () {
  it('challenges', function () {
    var nr1 = new NameRegistry(testData.pair1.privateKey);
    var nr2 = new NameRegistry(testData.pair2.privateKey);
    
    nr1.authorizeNamespace('n1', testData.pair2.publicKey);
    
    var message = nr1.createChallenge('n1');
    
    var response = nr2.answerChallenge(message);
    
    assert.equal('n1', nr1.verifyChallenge(message, response));
  })
})