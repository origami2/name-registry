var crypto = require('origami-crypto-utils');
var shortid = require('shortid');
var debug = require('debug')('origami:NameRegistry');

function NameRegistry(privateKey) {
  if (!privateKey) throw new Error('private key is required');

  this.key = crypto.asPrivateKey(privateKey);
  this.namespaces = {};
  this.publicNamespaces = [];
}

NameRegistry.prototype.getPublicKey = function () {
  debug('.getPublicKey');

  return this.key.exportKey('pkcs1-pem-public');
};

NameRegistry.prototype.isKnownNamespace = function (namespace) {
  debug('.isKnownNamespace');

  return !!this.namespaces[namespace] || this.publicNamespaces.indexOf(namespace) !== -1;
};

NameRegistry.prototype.listKnownNamespaces = function () {
  debug('.listKnownNamespaces');

  return Object.keys(this.namespaces).concat(this.publicNamespaces);
};

NameRegistry.prototype.addPublicNamespace = function (namespace) {
  if (!namespace) throw new Error('namespace is required');
  
  this.publicNamespaces.push(namespace);
};

NameRegistry.prototype.answerChallenge = function (message) {
  debug('.answerChallenge');

  try {
    if (!message) throw new Error('message is required');
    if (!message.code) throw new Error('message.code is required');
    if (!message.namespace) throw new Error('message.namespace is required');
    if (!message.encryptFor) throw new Error('message.encryptFor is required');

    return {
      data: crypto.encryptAndSign(
        [
          message.code,
          message.namespace
        ],
        this.key,
        message.encryptFor
      ),
      signedBy: this.getPublicKey()
    };
  } catch (e) {
    debug('.answerChallenge error %s', e.message || e);

    throw e;
  }
};

NameRegistry.prototype.createChallenge = function (namespace) {
  try {
    debug('.createChallenge %s', namespace);

    if (!namespace) throw new Error('namespace is required');

    var code = shortid.generate();

    return {
      code: code,
      encryptFor: this.getPublicKey(),
      namespace: namespace,
      token: crypto.encrypt([code, namespace], this.getPublicKey())
    };
  } catch (e) {
    debug('.createChallenge error %s', e.message || e);

    throw e;
  }
};

NameRegistry.prototype.authorizeNamespace = function (namespace, publicKey) {
  debug('.authorizeNamespace %s', namespace);

  if (!namespace) throw new Error('namespace is required');
  if (!publicKey) throw new Error('public key is required');

  this.namespaces[namespace] = crypto.asPublicKey(publicKey);
};

NameRegistry.prototype.verifyChallenge = function (message, response) {
  debug('.verifyChallenge %s/%s', JSON.stringify(message), JSON.stringify(response));

  try {
    if (!message) throw new Error('message is required');
    if (!message.code) throw new Error('message.code is required');
    if (!message.namespace) throw new Error('message.namespace is required');
    if (!response) throw new Error('response is required');

    try {
      var decrypted = crypto.decryptAndVerify(
        response.data,
        this.key,
        this.namespaces[message.namespace]
      );

      if (decrypted[0] === message.code &&
           decrypted[1] === message.namespace) {
         return message.namespace;
      } else return false;
    } catch (e) {
      return false;
    }
  } catch (e) {
    debug('.verifyChallenge error: %s', e.message || e);

    throw e;
  }
};

NameRegistry.prototype.getNamespacePublicKey = function (namespace) {
  if (this.publicNamespaces.indexOf(namespace) !== -1) return;
  if (!this.namespaces[namespace]) throw new Error('unknown namespace');
  
  return crypto.asString(this.namespaces[namespace]);
};

module.exports = NameRegistry;
